<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunPages;

class Init
{
    private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function init()
    {
        $this->registerMiddlewares();
        $this->registerViews();
        $this->registerUserRoute();
        $this->registerAdminRoute();
    }

    public function getModuleName()
    {
        return 'Pages';
    }

    public function getModuleAccessor()
    {
        return 'pages';
    }

    public static function getAdminUrl()
    {
        return '/admin/pages';
    }

    public function registerUserMenu()
    {
        $v = $this->app->getContainer()->get('view');
        return $v->fetch('@pages/menuUser.html.twig');
    }

    public static function registerAdminMenu($menu, $url)
    {
//        $settings = $c->get('settings');
//        $adminMenu = $c->get('menu')->get('admin_sidebar');

        $pageMenu = $menu->createItem('pages', array(
            'label' => 'Pages',
            'icon'  => 'book',//'file-text',
            'url'   => '#'
        ));
        $pageMenu->setAttribute('class', 'nav nav-second-level');

        $dashboardMenu = $menu->createItem('pages-dashboard', array(
            'label' => 'Dashboard',
            'icon'  => 'dashboard',
            'url'   => $url.'/admin/pages'
        ));

        $configMenu = $menu->createItem('pages-config', array(
            'label' => 'Configuration',
            'icon'  => 'cog',
            'url'   => $url.'/admin/pages/config'
        ));

        $pageMenu->addChildren('pages-dashboard', $dashboardMenu);
        $pageMenu->addChildren('pages-config', $configMenu);

        $menu->addItem('pages', $pageMenu);
    }

    private function registerMiddlewares()
    {
//        $this->app->add(new BBMiddleware($this->app));
    }

    private function registerViews()
    {
        // register template path & template alias
        $viewLoader = $this->app->getContainer()->get('view')->getLoader();
        $viewLoader->addPath(__DIR__ . '/Views', 'pages');
    }

    private function registerAdminRoute()
    {
//$this->DBG($this->app->getContainer()->get('router'));
        $this->app->group(self::getAdminUrl(), function () {
            $this->map(['GET', 'POST'], '', 'RunPages\Controllers\AdminIndex:go');
            $this->map(['GET', 'POST'], '/config', 'RunPages\Controllers\AdminConfig:go');
            $this->get('/{page_id}', 'RunPages\Controllers\AdminIndex:edit');
        });
    }

    private function registerUserRoute()
    {
        $this->app->group('/pages', function () {
            $this->get('[/{slug}]', 'RunPages\Controllers\PageController:getPage')->setName('page');
        });
    }
}