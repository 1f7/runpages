<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunPages\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;
use Illuminate\Database\Capsule\Manager as DB;

class AdminIndex extends AbstractController
{
    public function go(Request $request, Response $response, $params)
    {
        // Load page js BEFORE init !!!
        $this->core->loadJs('run-pages.js');
//        $this->core->loadCss('/themes/theme2/global.css', ['location' => 'external']);
//        $this->core->loadCss('/themes/theme2/css3.css', ['location' => 'external']);

        // init RunBB Admin
        $this->adm->init();
        // set active menu
        $this->bb->menu->get('admin_sidebar')->setActiveMenu('pages-dashboard');

//$this->bb->DBG($this->core->data);
        $data = [];
        $data['pagetitle'] = 'Pages List';

        //page_id
        //slug
        //title
        //created_at
        //updated_at

        $clause = [
            'limit' => 10,
            'uri' => $request->getUri()->getPath(),
            'order_by' => 'id',
            'order_direction' => 'desc'
        ];

        //wysiwyg
        $this->view->offsetSet('editor',
            $this->editor->build_mycode_inserter('page_body')
        );
        $data['pages'] = (new \RunPages\Models\Page)->paginate($this, $clause);
//$this->bb->DBG($data['pages']);

        return $this->view->render($response, '@pages/Admin/index.html.twig', $data);
    }

    public function edit(Request $request, Response $response, $params)
    {
        // init RunBB Admin
        $this->adm->init();
        $page = null;
        $message = '';

        try {
            $page = \RunPages\Models\Page::with('children')
                ->where('id', $params['page_id'])
                ->get();
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
//$this->bb->DBG($page[0]['children'][0]);
//        $data['pagetitle'] = 'Edit Page';
//        $data['page'] = $page[0]['children'][0];
//        return $this->view->render($response, '@pages/Admin/PageForm.html.twig', $data);

        //FIXME json or normal page????
        return $response->withJson([
            'success' => !is_null($page[0]['children'][0]),
            'data' => !is_null($page[0]['children'][0]) ? $page[0]['children'][0] : null,
            'message' => $message,
            'code' => is_null($page[0]['children'][0]) ? 404 : 200
        ]);
    }
}