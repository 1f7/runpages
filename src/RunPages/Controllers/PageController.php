<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunPages\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

use Faker;
use RunTracy\Helpers\TracyTrait;

class PageController extends AbstractController
{
    use TracyTrait;

    public function getPage(Request $request, Response $response, $params)
    {
//        $route = $request->getAttribute('route');

//        $this->fake();

        if(isset($params['slug'])) {
            $data = \RunPages\Models\PagesTranslation::where(['slug' => $params['slug']])->get();
            if($data->isEmpty()) {
                $notFoundHandler = $this->notFoundHandler;
                return $notFoundHandler($request, $response);
            }
        } else {
//            $clause = [
//                'limit' => 5,
//                'uri' => $request->getUri()->getPath(),
//                'order_by' => 'created_at',
//                'order_direction' => 'asc'
//            ];

            $data = (new \RunPages\Models\Page())->paginate($this);//, $clause);
        }

//$this->DBG($data);
        return $this->view->render($response, '@pages/index.html.twig', ['data' => $data]);
    }

    private function fake()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 50; $i++) {
            $page = \RunPages\Models\Page::create();
            $dbdata = [
                'id' => $page->id,
                'locale' => 'en',
                'slug' => 'test_' . $page->id,
                'title' => $faker->name,
                'body' => $faker->text($maxNbChars = 1200)
            ];
            $page->children()->create($dbdata);
        }
    }
}