<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunPages\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'meta_robots_no_index',
        'meta_robots_no_follow',
        'position',
        'parent_id',
        'private',
        'is_home',
        'redirect',
        'no_cache',
        'css',
        'js',
        'module',
        'template',
        'image',
    ];

    /**
     * A page can have children.
     */
    public function children()
    {
        return $this->hasMany('RunPages\Models\PagesTranslation', 'page_id');
    }

    /**
     * A page can have a parent.
     */
    public function parent()
    {
        return $this->belongsTo('RunPages\Models\Page', 'parent_id');
    }

    public static function pageByAlias($alias)
    {
        return self::where('alias', '=', $alias)
            ->get(1);
    }

    public function paginate(& $app, & $clause = [])
    {
        if(empty($clause)) {
            $clause = [
                'limit' => 10,
                'uri' => $app->request->getUri()->getPath(),
                'order_by' => 'id',
                'order_direction' => 'asc'
            ];
        }
        $pagescount = \RunPages\Models\Page::count();
        if ($app->bb->get_input('page', 0) > 0) {
            $page = $app->bb->get_input('page', 0);
            $start = ($page - 1) * $clause['limit'];
            $pages = $pagescount / $clause['limit'];
            $pages = ceil($pages);
            if ($page > $pages || $page <= 0) {
                $start = 0;
                $page = 1;
            }
        } else {
            $start = 0;
            $page = 1;
        }

        $app->view->offsetSet('multipage',
            $app->pagination->multipage($pagescount, $clause['limit'], $page, $clause['uri'])
        );

        return \RunPages\Models\PagesTranslation::orderBy($clause['order_by'], $clause['order_direction'])
            ->skip($start)
            ->take($clause['limit'])
            ->get();
    }
}