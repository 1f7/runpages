<?php use Illuminate\Database\Capsule\Manager as DB;

class AddForeignKeysToPagesSectionsTranslationsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->table('pages_sections_translations', function($table)
        {
            $table->foreign('section_id', 'pages_sections_translations_section_id_foreign')->references('id')->on('pages_sections')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->table('pages_sections_translations', function($table)
        {
            $table->dropForeign('pages_sections_translations_section_id_foreign');
        });
    }
}