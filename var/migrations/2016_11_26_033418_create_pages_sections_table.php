<?php use Illuminate\Database\Capsule\Manager as DB;

class CreatePagesSectionsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('pages_sections', function($table)
        {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->integer('position')->unsigned()->default(0);
            $table->integer('parent_id')->unsigned()->nullable()->index('pages_sections_parent_id_foreign');
            $table->boolean('private')->default(0);
            $table->boolean('is_home')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('pages_sections');
    }
}