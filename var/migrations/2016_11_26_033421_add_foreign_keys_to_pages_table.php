<?php use Illuminate\Database\Capsule\Manager as DB;

class AddForeignKeysToPagesTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->table('pages', function($table)
        {
            $table->foreign('parent_id', 'pages_parent_id_foreign')->references('id')->on('pages')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->table('pages', function($table)
        {
            $table->dropForeign('pages_parent_id_foreign');
        });
    }
}