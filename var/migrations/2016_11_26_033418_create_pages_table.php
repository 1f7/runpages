<?php use Illuminate\Database\Capsule\Manager as DB;

class CreatePagesTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('pages', function($table)
        {
            $table->increments('id');
            $table->string('meta_robots_no_index')->default('0');
            $table->string('meta_robots_no_follow')->default('0');
            $table->string('image')->nullable();
            $table->integer('position')->unsigned()->default(0);
            $table->integer('parent_id')->unsigned()->nullable()->index('pages_parent_id_foreign');
            $table->boolean('private')->default(0);
            $table->boolean('is_home')->default(0);
            $table->boolean('redirect')->default(0);
            $table->boolean('no_cache')->default(0);
            $table->text('css', 65535)->nullable();
            $table->text('js', 65535)->nullable();
            $table->string('module')->nullable();
            $table->string('template')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('pages');
    }
}