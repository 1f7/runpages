<?php use Illuminate\Database\Capsule\Manager as DB;

class AddForeignKeysToPagesSectionsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->table('pages_sections', function($table)
        {
            $table->foreign('parent_id', 'pages_sections_parent_id_foreign')->references('id')->on('pages_sections')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->table('pages_sections', function($table)
        {
            $table->dropForeign('pages_sections_parent_id_foreign');
        });
    }
}