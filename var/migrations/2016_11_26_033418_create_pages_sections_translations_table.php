<?php use Illuminate\Database\Capsule\Manager as DB;

class CreatePagesSectionsTranslationsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('pages_sections_translations', function($table)
        {
            $table->increments('id');
            $table->integer('section_id')->unsigned();
            $table->string('locale')->index('pages_sections_translations_locale_index');
            $table->string('slug')->nullable();
            $table->string('uri')->nullable();
            $table->string('title');
            $table->text('body', 65535);
            $table->timestamps();
            $table->unique(['section_id','locale'], 'pages_sections_translations_section_id_locale_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('pages_sections_translations');
    }
}