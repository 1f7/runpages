<?php use Illuminate\Database\Capsule\Manager as DB;

class CreatePagesTranslationsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('pages_translations', function($table)
        {
            $table->increments('id');
            $table->integer('page_id')->unsigned();
            $table->string('locale')->index('pages_translations_locale_index');
            $table->string('slug')->nullable();
            $table->string('uri')->nullable();
            $table->string('title');
            $table->text('body', 65535);
            $table->boolean('status')->default(0);
            $table->string('meta_title')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();
            $table->timestamps();
            $table->unique(['page_id','locale'], 'pages_translations_page_id_locale_unique');
            $table->unique(['locale','uri'], 'pages_translations_locale_uri_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('pages_translations');
    }
}