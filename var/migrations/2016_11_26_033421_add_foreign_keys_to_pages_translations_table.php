<?php use Illuminate\Database\Capsule\Manager as DB;

class AddForeignKeysToPagesTranslationsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->table('pages_translations', function($table)
        {
            $table->foreign('page_id', 'pages_translations_page_id_foreign')->references('id')->on('pages')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->table('pages_translations', function($table)
        {
            $table->dropForeign('pages_translations_page_id_foreign');
        });
    }
}