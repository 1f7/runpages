[![Latest Version on Packagist][ico-version]][link-packagist] [![Software License][ico-license]][link-license] [![Total Downloads][ico-downloads]][link-downloads]
#RunPages
RunCMF Pages package.  
`bla-bla info here`  

## NOW DEV ONLY. NO SUPPORT
  
## Install
* Via Composer command line
``` bash
$ composer require runcmf/runpages:dev-master
```
* Via composer.json
```
...
"require": {
    "runcmf/runpages":  "dev-master"
  },
...
```
``` bash
$ composer update
```

* copy or `ln -s` run-pages.js to site_root/web/assets/js

* migrate
``` bash
php cli migrate:fill vendor/runcmf/runpages
php cli seed:fill vendor/runcmf/runpages
```

![example](ss/ss1.png "simple example")


## Security

If you discover any security related issues, please email to 1f7.wizard( at )gmail.com or create an issue.

## Credits

* https://bitbucket.org/1f7
* https://github.com/1f7
* http://runetcms.ru
* http://runcmf.ru

## License

[Apache License Version 2.0](LICENSE.md)

[ico-version]: https://img.shields.io/packagist/v/runcmf/runpages.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-Apache%202-green.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/runcmf/runpages.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/runcmf/runpages
[link-license]: http://www.apache.org/licenses/LICENSE-2.0
[link-downloads]: https://bitbucket.org/1f7/runpages
