$(function(){
    /**
     * all response will be in below format
     * {
     *     success : boolean,
     *     data : {resource_object} or null,
     *     message : string,
     *     code : integer
     * }
     */

    var $loader = $('#loader');

    /**
     * reset the form and show it!
     */
    $('#btn-page-add').click(function(e){
        e.preventDefault();
        $('#page-form-data').each(function(){
            this.reset();
        });
        $('#btn-page-save').attr('data-method', 'POST');
        $('#page-modal').modal('show');
    });

    /**
     * sen GET request to display resource with specific id, and display it in modal form
     */
    $('#page-table').on('click', '.btn-page-edit', function(e){
        var $pageid = $(this).attr('data-id');

        e.preventDefault();
        $loader.show();

        $.get('/admin/pages/'+$pageid, function(resp){
            if(resp.success){
                $('#page-form-data').each(function(){
                    this.reset();
                });
                var $page = resp.data;
                for(var a in $page){
                    $('#page_'+a).val($page[a]).delay( 10 );
                }

                $('#btn-page-save').attr('data-method', 'PUT');
                $('#page-modal').modal('show');
            }else{
                alert(resp.message);
                if(resp.code == 401){
                    location.reload();
                }
            }

            $loader.hide();
        });
    });

    /**
     * send DELETE request to the resouce server
     */
    $('#page-table').on('click', '.btn-page-delete', function(e){
        var $pageid = $(this).attr('data-id');
        e.preventDefault();

        if(confirm('Are you sure to delete this page?')){
            $loader.show();
            $.ajax({
                url    : '/admin/pages/'+$pageid,
                method : 'DELETE',
                data   : {
                    id : $pageid
                },
                success : function(resp){
                    if(resp.success){
                        $('#page-row-'+$pageid).remove();
                    }else{
                        alert(resp.message);
                        if(resp.code == 401){
                            location.reload();
                        }
                    }
                    $loader.hide();
                }
            });
        }
    });

    /**
     * send POST request to save data to resource server
     * or send PUT request to update data on resource server
     * based on data-method value
     */
    $('#btn-page-save').click(function(e){
        e.preventDefault();

        var $button = $(this),
            $pagedata = $('#page-form-data').serialize(),
            $method = $(this).attr('data-method'),
            $url = ($method == 'POST') ? '/admin/pages' : '/admin/pages/'+$('#page_id').val();

        $button.prop('disabled', true);
        $button.html('saving...');
        $loader.show();

        $.ajax({
            url: $url,
            data: $pagedata,
            method : $method,
            success: function(resp){

                $button.prop('disabled', false);
                $button.html('save');
                $loader.hide();

                if(resp.success){

                    page = resp.data;

                    if($method == 'POST'){
                        /** append page to new row */
                        $('#page-table').append(
                            '<tr id="page-row-'+resp.data.id+'">'+
                            '<td>'+page.id+'</td>'+
                            '<td>'+page.first_name+'</td>'+
                            '<td>'+page.last_name+'</td>'+
                            '<td>'+page.email+'</td>'+
                            '<td class="text-center">'+
                            '<a data-id="'+page.id+'" class="btn btn-xs btn-primary btn-page-edit" href="#"><i class="fa fa-edit fa-fw"></i>Edit</a>'+
                            '<a data-id="'+page.id+'" class="btn btn-xs btn-danger btn-page-delete" href="#" style="margin-left: 5px"><i class="fa fa-times fa-fw"></i>Remove</a>'+
                            '</td>'+
                            '</tr>'
                        );
                    }else{
                        var $fields = $('#page-row-'+resp.data.id+' td');
                        $($fields[1]).html(page.first_name);
                        $($fields[2]).html(page.last_name);
                        $($fields[3]).html(page.email);
                    }

                    /** reset the form and hide modal form */
                    $('#page-form-data').each(function(){
                        this.reset();
                    });
                    $('#page-modal').modal('hide');
                }else{
                    alert(resp.message);
                    if(resp.code == 401){
                        location.reload();
                    }
                }
            }
        });
    });
});